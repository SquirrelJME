// -*- Mode: Java; indent-tabs-mode: t; tab-width: 4 -*-
// ---------------------------------------------------------------------------
// Multi-Phasic Applications: SquirrelJME
//     Copyright (C) Stephanie Gawroriski <xer@multiphasicapps.net>
// ---------------------------------------------------------------------------
// SquirrelJME is under the Mozilla Public License Version 2.0.
// See license.mkd for licensing and copyright information.
// ---------------------------------------------------------------------------

package net.multiphasicapps.jsr353;

/**
 * This is a scope for generation
 *
 * @since 2014/08/07
 */
enum GeneratorScope
{
	/** Object scope. */
	OBJECT,
	
	/** Array scope. */
	ARRAY,
	
	/** End. */
	;
}
